package file

import (
	"flag"
	"fmt"
	"os"
	"os/exec"
)

var dd = struct {
	cmd     string
	inputF  string
	input   string
	outputF string
	output  string
	bsF     string
	bs      string
	convF   string
	conv    string
	statusF string
	status  string
}{
	cmd:     "dd",
	inputF:  "if=",
	outputF: "of=",
	bsF:     "bs=",
	bs:      "4M",
	convF:   "conv=",
	conv:    "fsync",
	statusF: "status",
	status:  "progress",
}

const ddUsage = `dd command wrapper
Usage of duplicate-data:
	-i, --input string
	      file(ISO image) or device
	-o, --output string
	      file or device
	-b, --bytes string
	      read and write up to BYTES bytes at a time (default "4M")
`

func DD(args []string) {
	fs := flag.NewFlagSet("dd", flag.ExitOnError)

	fs.Usage = func() {
		fmt.Printf(ddUsage)
	}

	fs.StringVar(&dd.input, "i", "", "input file or device")
	fs.StringVar(&dd.input, "input", "", "input file")

	fs.StringVar(&dd.output, "o", "", "ouput file or device")
	fs.StringVar(&dd.output, "output", "", "full path device name")

	fs.StringVar(&dd.bs, "b", dd.bs, "read and write up to BYTES bytes at a time")
	fs.StringVar(&dd.bs, "bytes", dd.bs, "read and write up to BYTES bytes at a time")

	fs.Parse(args)

	// flag validation
	if len(args) == 0 {
		fs.Usage()
		os.Exit(0)
	}

	// input
	if dd.input == "" || dd.output == "" {
		fmt.Println("input or output device were not specified")
		fs.Usage()
		os.Exit(1)
	}

	duplicateData()
}

func duplicateData() error {
	input := dd.inputF + dd.input
	output := dd.outputF + dd.output

	//TODO: verify if status=progress shows output
	cmd := exec.Command(dd.cmd, input, dd.bsF, dd.bs, dd.convF,
		dd.conv, dd.statusF, dd.status, output)

	// show dd output in real time
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	if err := cmd.Run(); err != nil {
		return err
	}

	return nil
}
