package video

import (
	"flag"
	"fmt"
	"os/exec"

	//"log"
	"os"
)

//  if err := IsCommandAvailable(ffmpeg.cmd); err != nil {
//      return err
//  }

//  if err := compressVideo(); err != nil {
//      return err
//  }

const compressUsage = `
Usage of compress-video:
  -i, --input string
        Video file to convert
  -a, --acodec string
        audio codec (default "aac")
  -c, --crf string
        Constant Rate Factor (default "28")
  -v, --vcodec string
        video codec (default "libx265")
  -w, --whatsapp
        Produces a WhatsApp compatible video
`

func Compress(args []string) {
	//if debug {
	//	log.Printf("compressVideo got args: %v", args)
	//}

	ffmpeg := NewFfmpegCmd()

	// Defaults for best compression
	crf := "28"
	acodec := "aac"
	vcodec := "libx265"
	whatsappCodec := "libx264"

	fs := flag.NewFlagSet("compress-video", flag.ExitOnError)

	fs.Usage = func() {
		fmt.Printf(compressUsage)
	}

	fs.StringVar(&ffmpeg.videoI, "i", "", "Video file to convert")
	fs.StringVar(&ffmpeg.videoI, "input", "", "Video file to convert")

	fs.StringVar(&ffmpeg.crf, "c", crf, "Constant Rate Factor")
	fs.StringVar(&ffmpeg.crf, "crf", crf, "Constant Rate Factor")

	fs.StringVar(&ffmpeg.vcodec, "v", vcodec, "video codec")
	fs.StringVar(&ffmpeg.vcodec, "vcodec", vcodec, "video codec")

	fs.StringVar(&ffmpeg.acodec, "a", acodec, "audio codec")
	fs.StringVar(&ffmpeg.acodec, "acodec", acodec, "audio codec")

	fs.BoolVar(&ffmpeg.whatsapp, "w", false, "Produces a WhatsApp compatible video")
	fs.BoolVar(&ffmpeg.whatsapp, "whatsapp", false, "Produces a WhatsApp compatible video")

	fs.Parse(args)

	// flag validation
	if len(args) == 0 {
		fs.Usage()
		os.Exit(0)
	}

	// input
	if ffmpeg.videoI == "" {
		fmt.Println("No input file specified")
		fs.Usage()
		os.Exit(1)
	}

	if ffmpeg.whatsapp {
		ffmpeg.vcodec = whatsappCodec
	}

	compressUsing(ffmpeg)
}

func compressUsing(ffmpeg ffmpeg) error {
	outputFile := "new-" + ffmpeg.videoI

	// if debug
	//fmt.Println(ffmpeg.cmd, ffmpeg.input, ffmpeg.crf, ffmpeg.vcodec, ffmpeg.acodec, outputFile)

	cmd := exec.Command(ffmpeg.cmd, ffmpeg.inputF, ffmpeg.videoI,
		ffmpeg.crfF, ffmpeg.crf, ffmpeg.vcodecF, ffmpeg.vcodec,
		ffmpeg.acodecF, ffmpeg.acodec, ffmpeg.hideBan, outputFile)

	// show ffmpeg output in real time
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	if err := cmd.Run(); err != nil {
		return err
	}

	return nil
}
