package video

/*
   ffmpeg needs each flag and value to be passed one by one instead of a whole string.
   Otherwise the error: 'Error splitting the argument list' will happen.
   see: https://stackoverflow.com/questions/42532106/ffmpeg-error-splitting-the-argument-list-option-not-found

   therefore the <var>F represents the flags ffmpeg needs

	 Also despite ffmpeg can accept multiple input files, for minion' scope only two are permitted.
*/

type ffmpeg struct {
	cmd      string
	crfF     string
	crf      string
	inputF   string
	videoI   string
	audioI   string
	vcodecF  string
	vcodec   string
	acodecF  string
	acodec   string
	hideBan  string
	copyVF   string
	copyAF   string
	whatsapp bool
}

// define options global options and flags, eg `-i` for inputF
func NewFfmpegCmd() ffmpeg {
	return ffmpeg{
		cmd:     "ffmpeg",
		hideBan: "-hide_banner",
		inputF:  "-i",
		crfF:    "-crf",
		acodecF: "-acodec",
		vcodecF: "-vcodec",
		copyVF:  "-c:v",
		copyAF:  "-c:a",
	}
}
