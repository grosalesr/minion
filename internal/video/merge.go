package video

import (
	"flag"
	"fmt"
	"os"
	"os/exec"
)

// https://superuser.com/questions/277642/how-to-merge-audio-and-video-file-in-ffmpeg
const joinUsage = `Merge two input sources, audio and video, together
Usage of merge-audio-video:
  -v, --video string
        Video input
  -a, --audio string
        audio input
`

func Merge(args []string) {
	ffmpeg := NewFfmpegCmd()
	ffmpeg.acodec = "aac"

	fs := flag.NewFlagSet("join-video", flag.ExitOnError)

	fs.Usage = func() {
		fmt.Printf(joinUsage)
	}

	fs.StringVar(&ffmpeg.videoI, "v", "", "Video input")
	fs.StringVar(&ffmpeg.videoI, "video", "", "Video input")

	fs.StringVar(&ffmpeg.audioI, "a", "", "Audio input")
	fs.StringVar(&ffmpeg.audioI, "audio", "", "Audio input")

	// flag validation
	if len(args) == 0 {
		fs.Usage()
		os.Exit(0)
	}

	if ffmpeg.videoI == "" || ffmpeg.audioI == "" {
		fmt.Println("No input file specified")
		fs.Usage()
	}

	mergeUsing(ffmpeg)
}

func mergeUsing(ffmpeg ffmpeg) error {
	videoMode := "copy"

	// If audio or video stream is longer, this option will stop encoding once one file ends.
	shortest := "-shortest"
	outputFile := "new-" + ffmpeg.videoI

	cmd := exec.Command(ffmpeg.cmd,
		ffmpeg.inputF, ffmpeg.videoI, ffmpeg.copyVF, videoMode,
		ffmpeg.inputF, ffmpeg.audioI, ffmpeg.copyAF, ffmpeg.acodec,
		shortest, ffmpeg.hideBan, outputFile)

	// show ffmpeg output in real time
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	if err := cmd.Run(); err != nil {
		return err
	}

	return nil
}
