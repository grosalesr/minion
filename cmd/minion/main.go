package main

import (
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"

	"gitlab.com/grosalesr/minion/internal/file"
	"gitlab.com/grosalesr/minion/internal/video"
)

var commands = map[string]func([]string){
	"merge-audio-video": video.Merge,
	"compress-video":    video.Compress,
	"duplicate-data":    file.DD,
}

func printUsage() {
	fmt.Print("Usage: minion [global options] [command] [options]\n\n")
	fmt.Println("Available commands:")

	cmds := ""
	for k := range commands {
		cmds += "  " + "-" + k + "\n"
	}
	fmt.Println(cmds)

	fmt.Println("Global Options")
	flag.PrintDefaults()

}

func IsCommandAvailable(cmd_name string) error {
	cmd := exec.Command("/bin/sh", "-c", "command -v "+cmd_name)

	if err := cmd.Run(); err != nil {
		return errors.New("command not found: " + cmd_name)
	}

	return nil
}

func main() {
	// define and parse global options
	debug := flag.Bool("debug", false, "print debug information")
	flag.Parse()

	// parse subcommands
	subcommand := flag.Args()

	// check if there are subcommands to execute
	if len(subcommand) == 0 {
		printUsage()
		os.Exit(0)
	}

	cmd, ok := commands[subcommand[0]]
	if !ok {
		printUsage()
		os.Exit(1)
	}

	if *debug {
		log.Println("Debug info enabled")
	}

	cmd(subcommand[1:])
}
