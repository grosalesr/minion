build:
	go build -o bin/minion cmd/minion/main.go

run:
	go run cmd/minion/main.go
