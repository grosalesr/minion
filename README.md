minion
======

This projects have two main objectives:

1. Learn/practice Go by wrapping some linux commands that I don't often use and
1. Consolidate several bash scripts (with repeated structures) into a single executable file.

> **It is expected** that some subcommands offered by `minion` are merely wrappers with limited functionality exposed

`minion` will execute the appropriate (linux) command and `echo` its (std & stderr) streams.

Dependencies
------------

* dd
* ffmpeg

> They must be in system's $PATH

`minion` will verify the OS command exists before running any subcommand.
If the OS command does not exists, `minion` will fail.

Execution
--------

`minion [global options] [subcommand] [subcommand flags]`

## Global Options

* **debug**: prints extra information

## Sub Commands

### merge-audio-video
```
Usage of merge-audio-video:
  -v, --video string
        Video input
  -a, --audio string
        audio input
```

### compress-video
```
Usage of compress-video:
  -i, --input string
        Video file to convert
  -a, --acodec string
        audio codec (default "aac")
  -c, --crf string
        Constant Rate Factor (default "28")
  -v, --vcodec string
        video codec (default "libx265")
  -w, --whatsapp
        Produces a WhatsApp compatible video
```

### duplicate-data
```
Usage of duplicate-data:
	-i, --input string
	      file(ISO image) or device
	-o, --output string
	      file or device
	-b, --bytes string
	      read and write up to BYTES bytes at a time (default "4M")
```

License
-------

GPL-3.0+
